import { useEffect } from 'react';
import Header from '../Header/Header';
import './Chat.css';
import MessageList from '../MessageList/MessageList';
import MessageInput from '../MessageInput/MessageInput';
import { IMessage } from '../../types/IMessage';
import { useDispatch, useSelector } from 'react-redux';
import { chat as chatActionCreator } from '../../store/actions';

interface IChat {
  messages: IMessage[];
  editModal: boolean;
  preloader: boolean;
}

const Chat = (): JSX.Element => {
  const { messages } = useSelector(({ chat }: { chat: IChat }) => {
    return { messages: chat.messages };
  });

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(chatActionCreator.fetchMessages());
  }, []);

  const formatDate = (fullDate: string): string => {
    if (!fullDate) return '';

    const date = fullDate.split('T')[0];
    const time = fullDate.split('T')[1];
    const dateDay = date.slice(8, 10);
    const dateMonth = date.slice(5, 7);
    const dateYear = date.slice(0, 4);
    const formatedTime = time.slice(0, 5);

    return dateDay + '.' + dateMonth + '.' + dateYear + ' ' + formatedTime;
  };

  const sortListByDate = (messages: IMessage[]): IMessage[] => {
    const copyMessages = [...messages];
    const sortedMessages: IMessage[] = copyMessages.sort((msgOne, msgTwo) => {
      return +new Date(msgOne.createdAt) - +new Date(msgTwo.createdAt);
    });

    return sortedMessages;
  };

  const getUsersCount = (): number => {
    const uniqueUsers = new Set();
    messages.forEach((msg) => uniqueUsers.add(msg.userId));

    return uniqueUsers.size;
  };

  return (
    <main className="chat">
      <div className="container">
        <Header
          title="My chat"
          usersCount={getUsersCount()}
          messagesCount={messages.length}
          lastMessageDate={formatDate(
            sortListByDate(messages).slice(-1)[0]?.createdAt
          )}
        />
        <MessageList messages={sortListByDate(messages)} />
        <MessageInput />
      </div>
    </main>
  );
};

export default Chat;
