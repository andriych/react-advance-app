import './Header.css';

interface IHeaderProps {
  title: string;
  usersCount: number;
  messagesCount: number;
  lastMessageDate: string;
}

const Header = ({
  title,
  usersCount,
  messagesCount,
  lastMessageDate,
}: IHeaderProps): JSX.Element => {
  return (
    <header className="header">
      <div className="header-chat-info">
        <p className="header-title">{title}</p>
        <p className="header-users-count">{usersCount}</p>participants
        <p className="header-messages-count">{messagesCount}</p>messages
      </div>
      <div className="header-other-info">
        last message at
        <p className="header-last-message-date">{lastMessageDate.trim()}</p>
      </div>
    </header>
  );
};

export default Header;
