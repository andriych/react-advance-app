import { useState } from 'react';
import { getHoursMins } from '../../helpers/date-format';
import { IMessage } from '../../types/IMessage';
import './Message.css';

interface IMessageProps {
  message: IMessage;
}

const Message = ({ message }: IMessageProps): JSX.Element => {
  const { avatar, user, text, createdAt, editedAt } = message;

  const [isLiked, setIsLiked] = useState(false);

  const onLikeHandle = (): void => setIsLiked(!isLiked);

  return (
    <div className="message">
      <img src={avatar} alt="user" className="message-user-avatar" />
      <div className="message-info">
        <div className="message-details">
          <p className="message-user-name">{user}</p>

          {editedAt ? (
            <>
              edited at
              <p className="message-time">{`${getHoursMins(editedAt)}`}</p>
            </>
          ) : (
            <p className="message-time">{`${getHoursMins(createdAt)}`}</p>
          )}
        </div>
        <p className="message-text">{text}</p>
      </div>
      {isLiked ? (
        <img
          src="liked.png"
          alt="like"
          className="message-liked"
          onClick={onLikeHandle}
        />
      ) : (
        <img
          src="like.png"
          alt="like"
          className="message-like"
          onClick={onLikeHandle}
        />
      )}
    </div>
  );
};

export default Message;
