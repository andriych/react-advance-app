import { useDispatch, useSelector } from 'react-redux';
import { IMessage } from '../../types/IMessage';
import Message from '../Message/Message';
import Modal from '../Modal/Modal';
import OwnMessage from '../OwnMessage/OwnMessage';
import Preloader from '../Preloader/Preloader';
import { chat as chatActionCreator } from '../../store/actions';
import './MessageList.css';

enum Days {
  Sunday,
  Monday,
  Tuesday,
  Wednesday,
  Thursday,
  Friday,
  Saturday,
}

enum Months {
  January,
  February,
  March,
  April,
  May,
  June,
  July,
  August,
  September,
  October,
  November,
  December,
}

interface IMessageListProps {
  messages: IMessage[];
  // onMessageEdit: (id: string, text: string) => void;
  // onMessageDelete: (id: string) => void;
}

interface IChat {
  messages: IMessage[];
  editModal: boolean;
  preloader: boolean;
  editId: string;
}

const currentUserId = '1111243930-83c9-11e9-8e0c-8f1a686f4ce4';

const MessageList = ({ messages }: IMessageListProps): JSX.Element => {
  const dispatch = useDispatch();
  const { preloader, editModal, editId } = useSelector(
    ({ chat }: { chat: IChat }) => {
      return {
        preloader: chat.preloader,
        editModal: chat.editModal,
        editId: chat.editId,
      };
    }
  );

  const editedMessage = messages.find((msg) => msg.id === editId);
  const isDay = (date: Date, dayOffset = 0) => {
    const today = new Date();
    const utc1 = Date.UTC(date.getFullYear(), date.getMonth(), date.getDate());
    const utc2 = Date.UTC(
      today.getFullYear(),
      today.getMonth(),
      today.getDate()
    );
    const diffInDays = Math.floor((utc2 - utc1) / (1000 * 60 * 60 * 24));

    return !(diffInDays + dayOffset);
  };

  const getOutputDividerDate = (date: string): string => {
    let day = '';
    const currentDate = new Date(date);

    if (isDay(currentDate)) {
      day = 'Today';
    } else if (isDay(currentDate, -1)) {
      day = 'Yesterday';
    } else {
      day = `${Days[currentDate.getDay()]}, ${currentDate.getDate()} ${
        Months[currentDate.getMonth()]
      }`;
    }

    return day;
  };

  const getOutputDivider = (date: string): JSX.Element => {
    const day = getOutputDividerDate(date);
    return (
      <div className="messages-divider-wrapper">
        <span className="messages-divider">{day}</span>
        <div className="messages-divider-line"></div>
      </div>
    );
  };

  const getOutputMessage = (message: IMessage): JSX.Element => {
    const msg =
      message.userId !== currentUserId ? (
        <Message key={message.id} message={message} />
      ) : (
        <OwnMessage key={message.id} message={message} />
      );
    return msg;
  };

  const handleSaveMessage = (text: string) => {
    if (editedMessage) {
      dispatch(
        chatActionCreator.updateMessage({
          ...editedMessage,
          text,
          editedAt: new Date().toISOString(),
        })
      );
    }
  };

  if (preloader) {
    return (
      <div className="message-list">
        <Preloader />
      </div>
    );
  }

  let prevDividerDate = '';

  return (
    <>
      <div className="message-list">
        {messages.map((msg, i) => {
          if (prevDividerDate === getOutputDividerDate(msg.createdAt)) {
            return getOutputMessage(msg);
          } else {
            prevDividerDate = getOutputDividerDate(msg.createdAt);
            return (
              <div key={i}>
                {getOutputDivider(msg.createdAt)}
                {getOutputMessage(msg)}
              </div>
            );
          }
        })}
      </div>
      {editModal && editedMessage ? (
        <Modal message={editedMessage} onSave={handleSaveMessage} />
      ) : null}
    </>
  );
};

export default MessageList;
