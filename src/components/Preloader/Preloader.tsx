import './Preloader.css';

const Preloader = (): JSX.Element => {
  return <div className="preloader"></div>;
};

export default Preloader;
