import { createReducer } from '@reduxjs/toolkit';
import { IMessage } from '../../types/IMessage';
import {
  fetchMessages,
  updateMessage,
  createMessage,
  showEditModal,
} from './actions';

interface IChat {
  messages: IMessage[];
  editModal: boolean;
  preloader: boolean;
  editId: string;
}

const initialState: IChat = {
  messages: [],
  editModal: false,
  preloader: true,
  editId: '',
};

const reducer = createReducer(initialState, (builder) => {
  builder.addCase(fetchMessages.pending, (state) => {
    state.preloader = true;
  });
  builder.addCase(fetchMessages.fulfilled, (state, { payload }) => {
    const { messages } = payload;

    state.messages = messages;
    state.preloader = false;
  });
  builder.addCase(createMessage.fulfilled, (state, { payload }) => {
    const { message } = payload;

    state.messages = state.messages.concat(message);
  });
  builder.addCase(showEditModal.fulfilled, (state, { payload }) => {
    const { editModal, editId } = payload;
    state.editModal = editModal;
    state.editId = editId;
  });
  builder.addCase(updateMessage.fulfilled, (state, { payload }) => {
    const { message } = payload;
    state.messages = state.messages.map((msg) => {
      return msg.id === message.id ? { ...msg, ...message } : msg;
    });
  });
});

export { reducer };
