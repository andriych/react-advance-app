const ActionType = {
  FETCH_MESSAGES: 'chat/fetch-messages',
  UPDATE: 'chat/update',
  CREATE: 'chat/create',
  DELETE: 'chat/delete',
  MODAL: 'chat/modal',
};

export { ActionType };
