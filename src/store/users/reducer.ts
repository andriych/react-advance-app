import { createReducer } from '@reduxjs/toolkit';
import { IUser } from '../../types/IUser';
import { fetchUsers, authUser } from './actions';

const initialState: { users: IUser[]; currentUser: IUser | undefined } = {
  users: [],
  currentUser: undefined,
};

const reducer = createReducer(initialState, (builder) => {
  builder.addCase(fetchUsers.fulfilled, (state, { payload }) => {
    const { users } = payload;

    state.users = users;
  });
  builder.addCase(authUser.fulfilled, (state, { payload }) => {
    const { currentUser } = payload;

    state.currentUser = currentUser;
  });
});

export { reducer };
