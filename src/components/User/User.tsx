import { IUser } from '../../types/IUser';
import './User.css';

const User = ({ data }: { data: IUser }): JSX.Element => {
  return (
    <div className="user">
      <p>{data.login}</p>
      <p>{data.password}</p>
      <button className="edit-user-button">Edit</button>
      <button className="delete-user-button">Delete</button>
    </div>
  );
};

export default User;
