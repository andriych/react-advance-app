import { combineReducers } from '@reduxjs/toolkit';

import { reducer as chat } from './chat/reducer';
import { reducer as users } from './users/reducer';

export default combineReducers({
  chat,
  users,
});
