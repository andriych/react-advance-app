import { createAsyncThunk } from '@reduxjs/toolkit';
import { IMessage } from '../../types/IMessage';
import { ActionType } from './common';

const fetchMessages = createAsyncThunk(
  ActionType.FETCH_MESSAGES,
  async (_args) => {
    const res = await fetch('http://localhost:3004/messages', {
      method: 'GET',
    });
    const messages: IMessage[] = await res.json();
    return { messages };
  }
);

const updateMessage = createAsyncThunk(
  ActionType.UPDATE,
  async (payload: IMessage) => {
    const res = await fetch(`http://localhost:3004/messages/${payload.id}`, {
      method: 'PUT',
      headers: { 'Content-Type': 'application/json;charset=utf-8' },
      body: JSON.stringify(payload),
    });

    const message: IMessage = await res.json();
    return { message };
  }
);

const createMessage = createAsyncThunk(
  ActionType.CREATE,
  async (payload: IMessage) => {
    const res = await fetch('http://localhost:3004/messages/', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json;charset=utf-8' },
      body: JSON.stringify(payload),
    });

    const message = await res.json();
    return { message };
  }
);

// DELETING DOESN'T WORK BEACAUSE MESSAGE DATA CONTAINS 2 FIELD WITH ID AND IT
// CAUSES ERROR IN JSON-SERVER
// https://github.com/typicode/json-server/issues/13

// const deleteMessage = createAsyncThunk(
//   ActionType.DELETE,
//   async (payload: { id: string }) => {
//     const res = await fetch(`http://localhost:3004/messages/${payload.id}`, {
//       method: 'DELETE',
//     });

//     const message = await res.json();
//     return { message };
//   }
// );

const showEditModal = createAsyncThunk(
  ActionType.MODAL,
  (payload: { editModal: boolean; editId: string }) => {
    return { ...payload };
  }
);

export { fetchMessages, updateMessage, createMessage, showEditModal };
