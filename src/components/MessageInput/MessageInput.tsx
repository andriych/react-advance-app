import { useState } from 'react';
import { IMessage } from '../../types/IMessage';
import './MessageInput.css';
import { guid } from '../../helpers/generate-id';
import { useDispatch } from 'react-redux';
import { chat as chatActionCreator } from '../../store/actions';

const CURRENT_USER_ID = '1111243930-83c9-11e9-8e0c-8f1a686f4ce4';

const MessageInput = (): JSX.Element => {
  const [messageText, setMessageText] = useState('');

  const dispatch = useDispatch();

  const handleInput = (e: React.FormEvent<HTMLInputElement>) => {
    setMessageText(e.currentTarget.value);
  };

  const handleSendMessage = (text: string) => {
    const date = new Date();

    const message: IMessage = {
      id: guid(),
      userId: CURRENT_USER_ID,
      avatar:
        'https://resizing.flixster.com/kr0IphfLGZqni5JOWDS2P1-zod4=/280x250/v1.cjs0OTQ2NztqOzE4NDk1OzEyMDA7MjgwOzI1MA',
      user: 'Me',
      text: messageText,
      createdAt: date.toISOString(),
      editedAt: '',
    };

    dispatch(chatActionCreator.createMessage(message));
  };

  return (
    <form
      className="message-input"
      onSubmit={(e) => {
        e.preventDefault();
        handleSendMessage(messageText);
        setMessageText('');
      }}
    >
      <input
        className="message-input-text"
        type="text"
        onChange={handleInput}
        value={messageText || ''}
      />

      <button className="message-input-button" type="submit">
        Send
      </button>
    </form>
  );
};

export default MessageInput;
