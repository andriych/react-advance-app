import { useDispatch } from 'react-redux';
import { getHoursMins } from '../../helpers/date-format';
import { IMessage } from '../../types/IMessage';
import './OwnMessage.css';
import { chat as chatActionCreator } from '../../store/actions';

interface IOwnMessageProps {
  message: IMessage;
}

const OwnMessage = ({ message }: IOwnMessageProps): JSX.Element => {
  const { avatar, user, text, createdAt, editedAt } = message;

  const dispatch = useDispatch();

  const handleEditMessage = (message: IMessage) => {
    dispatch(
      chatActionCreator.showEditModal({ editModal: true, editId: message.id })
    );
  };

  const handleDeleteMessage = () => {};
  return (
    <div className="own-message">
      <button
        type="button"
        className="message-edit"
        onClick={() => handleEditMessage(message)}
      >
        ✏️
      </button>
      <button
        type="button"
        className="message-delete"
        onClick={() => handleDeleteMessage()}
      >
        ❌
      </button>
      <div className="message-info">
        <div className="message-details">
          <p className="message-user-name">{user}</p>
          {editedAt ? (
            <>
              edited at
              <p className="message-time">{`${getHoursMins(editedAt)}`}</p>
            </>
          ) : (
            <p className="message-time">{`${getHoursMins(createdAt)}`}</p>
          )}
        </div>
        <p className="message-text">{text}</p>
      </div>
      <img src={avatar} alt="user" className="message-user-avatar" />
    </div>
  );
};

export default OwnMessage;
