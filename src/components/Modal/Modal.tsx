import { useDispatch } from 'react-redux';
import './Modal.css';
import { chat as chatActionCreator } from '../../store/actions';
import { IMessage } from '../../types/IMessage';
import { useState } from 'react';

interface IModalProps {
  message: IMessage;
  onSave: (text: string) => void;
}

const Modal = ({ message, onSave }: IModalProps): JSX.Element => {
  const [text, setText] = useState(message.text);
  const dispatch = useDispatch();
  const handleCloseModal = (): void => {
    dispatch(chatActionCreator.showEditModal({ editModal: false, editId: '' }));
  };

  return (
    <div className="edit-message-modal">
      <form
        className="modal-shown"
        onSubmit={(e) => {
          e.preventDefault();
          onSave(text);
          handleCloseModal();
        }}
      >
        <p className="edit-message-title">Edit message</p>
        <textarea
          className="edit-message-input"
          cols={50}
          rows={10}
          value={text}
          onChange={(e) => {
            setText(e.target.value);
          }}
        />
        <div className="modal-control">
          <button type="submit" className="edit-message-button">
            OK
          </button>
          <button className="edit-message-close" onClick={handleCloseModal}>
            CANCEL
          </button>
        </div>
      </form>
    </div>
  );
};

export default Modal;
