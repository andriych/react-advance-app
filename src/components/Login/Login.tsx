import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { users as usersActionCreator } from '../../store/actions';

import './Login.css';
import { IUser } from '../../types/IUser';

enum UserStatus {
  FOUND = 'FOUND',
  NOTFOUND = 'NOTFOUND',
}

enum UserRole {
  ADMIN = 'ADMIN',
  USER = 'USER',
}

const Admin = {
  login: 'admin',
  password: 'admin',
};

interface IUsers {
  users: IUser[];
  currentUser: IUser;
}

const Login = (): JSX.Element => {
  const dispatch = useDispatch();

  const { users } = useSelector(({ users }: { users: IUsers }) => ({
    users: users.users,
    currentUser: users.currentUser,
  }));

  useEffect(() => {
    dispatch(usersActionCreator.fetchUsers());
  }, []);

  const navigate = useNavigate();

  const [userVerifData, setUserVerifData] = useState<IUser>({
    login: '',
    password: '',
  });

  const handleLoginChange = (value: string) => {
    setUserVerifData({ ...userVerifData, login: value });
  };

  const handlePasswordChange = (value: string) => {
    setUserVerifData({ ...userVerifData, password: value });
  };

  const verifyUser = (data: IUser): { status: string; role: string } => {
    const user = users.find(
      (user) => user.login === data.login && user.password === data.password
    );

    if (user) {
      if (user.login === Admin.login && user.password === Admin.password)
        return { status: UserStatus.FOUND, role: UserRole.ADMIN };

      return { status: UserStatus.FOUND, role: UserRole.USER };
    }

    return { status: UserStatus.NOTFOUND, role: '' };
  };

  const handleLogIn = (): void => {
    const verificaition = verifyUser(userVerifData);

    if (verificaition.status === UserStatus.FOUND) {
      if (verificaition.role === UserRole.ADMIN)
        navigate('/users', { replace: true });
      if (verificaition.role === UserRole.USER)
        navigate('/', { replace: true });
    } else {
      navigate('/login', { replace: true });
    }
  };

  return (
    <div className="login-container">
      <form className="login">
        <input
          type="text"
          className="login-login"
          onChange={(e) => handleLoginChange(e.target.value)}
        />
        <input
          type="password"
          className="login-password"
          onChange={(e) => handlePasswordChange(e.target.value)}
        />
        <button
          type="submit"
          className="login-button"
          onClick={(e) => {
            e.preventDefault();
            handleLogIn();
          }}
        >
          Log in
        </button>
      </form>
    </div>
  );
};

export default Login;
