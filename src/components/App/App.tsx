import './App.css';
import Chat from '../Chat/Chat';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import Login from '../Login/Login';
import UsersList from '../UsersList/UsersList';
const App = (): JSX.Element => {
  return (
    <Router>
      <Routes>
        <Route
          path="/"
          element={
            <div className="app">
              <Chat />
            </div>
          }
        />
        <Route path="/login" element={<Login />} />
        <Route path="/users" element={<UsersList />} />
      </Routes>
    </Router>
  );
};

export default App;
