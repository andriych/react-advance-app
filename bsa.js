import Chat from './src/components/Chat/Chat';
import rootReducer from './src/store/root-reducer';

export default {
  Chat,
  rootReducer,
};
