const ActionType = {
  FETCH_USERS: 'users/fetch-users',
  AUTH: 'users/auth',
};

export { ActionType };
