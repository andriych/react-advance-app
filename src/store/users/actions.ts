import { createAsyncThunk } from '@reduxjs/toolkit';
import { IUser } from '../../types/IUser';
import { ActionType } from './common';

const fetchUsers = createAsyncThunk(ActionType.FETCH_USERS, async () => {
  const res = await fetch('http://localhost:3004/users', {
    method: 'GET',
  });

  const users: IUser[] = await res.json();

  return { users };
});

const authUser = createAsyncThunk(ActionType.AUTH, async (payload: IUser) => {
  const res = await fetch(`http://localhost:3004/users`, {
    method: 'GET',
  });

  const users: IUser[] = await res.json();

  const currentUser = users.find(
    (user) => user.login === payload.login && user.password === payload.password
  );

  return { currentUser };
});
export { fetchUsers, authUser };
