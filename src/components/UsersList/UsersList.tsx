import { useDispatch, useSelector } from 'react-redux';
import { IUser } from '../../types/IUser';
import User from '../User/User';
import './UsersList.css';
import { users as usersActionCreator } from '../../store/actions';
import { useEffect } from 'react';

interface IUsers {
  users: IUser[];
  currentUser: IUser;
}

const UsersList = (): JSX.Element => {
  const dispatch = useDispatch();
  const { users } = useSelector(({ users }: { users: IUsers }) => ({
    users: users.users,
  }));

  useEffect(() => {
    dispatch(usersActionCreator.fetchUsers());
  }, []);

  return (
    <div className="users-list">
      <button className="add-user-button">Add user</button>
      {users.map((user, i) => {
        return <User key={i} data={user} />;
      })}
    </div>
  );
};

export default UsersList;
